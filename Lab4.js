function cuadradosycubos(){
    let tabla = prompt("Introduce un número", "1");
    let n = parseInt(tabla);
    document.write("<table border = '1'>");
	document.write("<thead><th>Número</th> <th>Cuadrados</th> <th>Cubos</th></thead>");

	for(let i = 1; i <= n; i++) {
		document.write("<tr><td> " + i + " </td> <td>" + i**2 + "</td> <td>" + i**3 + "</td></tr>");
	}
	document.write("</table>");
}

function suma(){
    let num1, num2;
    num1 = Math.floor(Math.random() * (100 - 1) + 1);
    num2 = Math.floor(Math.random() * (100 - 1) + 1);
    let hora1, hora2;
    hora1 = performance.now();
    let pregunta = prompt(num1 + " + " + num2, "");
    hora2 = performance.now();
    if (pregunta == num1+num2){
        document.write("Correcto!!!");
    } else {
        document.write("Incorrecto :(");
    }
    let tiempo = Math.ceil((hora2 - hora1)/ 1000);
    document.write("<br> Tardaste " + tiempo + " segundos.");
}

function contador(){
    let num = [];
    for (let i = 0; i < 20; i++){
        num[i] = Math.floor(Math.random()*20 - 10);
        document.write(num[i] + " ")
    }
    document.write("<br>");
    let neg = 0, cero = 0, pos = 0;
    for (let i = 0; i < num.length; i++){
        if (num[i] == 0){
            cero++;
        } else if (num[i] < 0){
            neg++;
        } else if (num[i] > 0){
            pos++;
        }
    }
    document.write("El arreglo tiene " + cero + " ceros, " + neg + " números negativos y " + pos + " números positivos.");
}

function promedios(){
    let num = [];
    let suma = [];
    for (let i = 0; i < 10; i++){
        suma[i] = 0;
    }
    for (let i = 0; i < 10; i++){
        num[i] = [];
        for (let j = 0; j < 10; j++){
            num[i][j] = Math.floor(Math.random()*10);
            document.write(num[i][j] + " ");
            suma[i] += num[i][j];
        }
        document.write("<br>");
        suma[i] = suma[i] / 10;
        document.write("Promedio: " + suma[i] + "<br>");
    }

}
    
function inverso(num){
    let num_s = num.toString();
    let string = "";
    for (let i = (num_s.length) - 1; i >= 0; i--){
        string += num_s.charAt(i);
    }
    document.write(string);
}

class Numero { 
    constructor(arr){
        this.arr = arr;
    }
    
    par(arr){
        for (let i = 0; i < arr.length; i++){
            if(arr[i]%2 == 0){
                document.write(arr[i] + " ");
            }
        }
    }
    
    impar(arr){
        for (let i = 0; i < arr.length; i++){
            if(arr[i]%2 != 0){
                document.write(arr[i] + " ");
            }
        }
    }
}

function arreglo(){
    let num = [];
    for (let i = 0; i < 20; i++){
        num[i] = Math.floor(Math.random()*10 + 1);
        document.write(num[i] + " ")
    }
    document.write("<br>");
    let numero = new Numero(num);
    numero.par(num);
    document.write("<br>");
    numero.impar(num);
    
}

